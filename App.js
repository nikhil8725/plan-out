import React from "react";
import Login from "./Views/Login.js";
import Home from "./Views/Home/Home";
import Settings from "./Views/Settings/Settings";
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View
} from "react-native";
import {
  StackNavigator,
  SwitchNavigator,
  DrawerNavigator
} from "react-navigation";
import AddQuestion from "./Views/AddQuestion/AddQuestion";
import SignUp from "./Views/SignUp";
import SelectFriendsAndGroups from "./Views/AddQuestion/SelectFriendsAndGroups";
import FriendsandGroup from './Views/FriendsAndGroups/FriendsandGroup.js';

class AuthLoadingScreen extends React.Component {
  constructor() {
    super();
    this._bootstrapAsync();
  }

  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem("userToken");

    this.props.navigation.navigate(userToken ? "App" : "Auth");
  };
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});

const HomeDrawer = DrawerNavigator(
  {
    Home: {
      screen: Home
    },
    FriendsAndGroups:{
      screen : FriendsandGroup
    },
    Settings: {
      screen: Settings
    },
   
  },
  {
    initialRouteName: "Home",
    drawerPosition: "left",
    drawerOpenRoute: "DrawerOpen",
    drawerCloseRoute: "DrawerClose",
    drawerToggleRoute: "DrawerToggle"
  }
);

const AppStack = StackNavigator({
  Home: HomeDrawer,
  AddQuestion: AddQuestion,
  SelectFriendsAndGroups: SelectFriendsAndGroups
});
const AuthStack = StackNavigator({
  SignIn: Login,
  SignUp: SignUp
});

export default SwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack
  },
  {
    initialRouteName: "AuthLoading"
  }
);
