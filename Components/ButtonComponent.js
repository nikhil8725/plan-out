import React from 'react';
import { StyleSheet, Button,Alert} from 'react-native';

export default class ButtonComponent extends React.Component {
    constructor(props){
        super(props);
    }
  render() {
    return (
        <Button title={this.props.buttonTitle} onPress= {this.props.handleClick}></Button>
    );
  }
}