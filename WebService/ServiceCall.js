export default function serviceCall(type, body){
  
      return fetch('http://ec2-18-191-53-132.us-east-2.compute.amazonaws.com:3000/signup', {
            method: type,
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: body,
          })
          .then(function(response){
            return response.json();
          })
          .then(function(json){
            return { json }
          })
          .catch(function(error) {
          console.warn('There has been a problem with your fetch operation: ' + error.message);
          // ADD THIS THROW error
            throw error;
      });
}