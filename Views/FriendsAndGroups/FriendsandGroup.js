import React, { Component } from "react";
import { Container, Input, Item, Icon, Grid, Row, Content } from "native-base";
import FriendsTab from "../AddQuestion/FriendsTab";
import GroupTab from "../AddQuestion/GroupTab";
import { TabNavigator, TabBarBottom } from "react-navigation";
import { MaterialDialog } from "react-native-material-dialog";
import { styles, View } from "react-native";

export default class FriendsandGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      dialogVisible: false
    };
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
    });
    this.setState({ loading: false });
  }

  componentWillMount() {
    console.log("componentWillMount");
    this.props.navigation.setParams({ _showDialog: this._showDialog });
  }

  _showDialog = () => {
    console.log("show dialog");
    this.setState({ dialogVisible: true });
  };

  static navigationOptions = ({ navigation }) => {
    return {
      title: "Friends and Group",
      headerLeft: (
        <Icon
          name="ios-menu"
          style={{ paddingLeft: 10 }}
          onPress={() => navigation.navigate("DrawerOpen")}
        />
      ),
      headerRight: (
        <Icon
          style={{ marginRight: 15, color: "red" }}
          name={"add-circle"}
          size={25}
          onPress={navigation.getParam("_showDialog")}
        />
      )
    };
  };

  render() {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }
    return (
      <Container>
        <Grid>
          <Row size={9.25}>
            <SelectFriendsAndGroupsStack />
          </Row>
        </Grid>
        <MaterialDialog
          title="Add Friend"
          visible={this.state.dialogVisible}
          onOk={() => this.setState({ dialogVisible: false })}
          onCancel={() => this.setState({ dialogVisible: false })}
        >
          <View>
            <Item rounded style={{ height: 40 }}>
              <Input placeholder="Name" />
            </Item>
            <Item rounded style={{ height: 40, marginTop: 10 }}>
              <Input placeholder="Mobile Number" />
            </Item>
          </View>
        </MaterialDialog>
      </Container>
    );
  }
}

SelectFriendsAndGroupsStack = TabNavigator(
  {
    Friends: { screen: FriendsTab },
    Groups: { screen: GroupTab }
  },
  {
    tabBarPosition: "top",
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === "Groups") {
          iconName = `contacts`;
          iconColor = focused ? "tomato" : "gray";
        } else if (routeName === "Friends") {
          iconName = "contact";
          iconColor = focused ? "tomato" : "gray";
        }
        return <Icon name={iconName} size={25} style={{ color: iconColor }} />;
      }
    }),
    tabBarComponent: TabBarBottom,
    tabBarOptions: {
      activeTintColor: "tomato",
      inactiveTintColor: "gray"
    },
    animationEnabled: true,
    swipeEnabled: false
  }
);
