import React  from 'react';
import { Text } from 'react-native';
import {Container, Grid, Row, Content, Form, Item, Input, Label, Button} from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';
import serviceCall from '../WebService/ServiceCall.js'



export default class SignUp extends React.Component{

    static navigationOptions = {
        title: 'Sign Up',
      };

      constructor(props) {
        super(props);
        this.state = {name : "",
         email: "", 
         number:"",
         password:"",
         visibleSpinner : false,
         success:{
             nameSuccess:false,
             mobSuccess:false,
             emailSuccess:false,
             passwordSuccess:false,
             confirmPasswordSuccess:false

        },
        validated : false
        };
      }

      componentWillUnmount(){
        clearInterval();
      }

      validate(text, type){
          alph = /^[a-zA-Z]+$/
          mobileNumber = /^[0-9]+$/
          emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


          if(type=='yourName'){
              if(alph.test(text) && (text.length > 0)){
                  //console.warn("Cool Name");
                  this.setState(prevState => ({
                    success: {
                        ...prevState.success,
                        nameSuccess: true
                    }}))
                  this.setState({name : text})                    
              }else{
               // console.warn("Bad Name");
                this.setState(prevState => ({
                    success: {
                        ...prevState.success,
                        nameSuccess: false
                    }}))
              }
          }else if(type=='mobileNumber'){
            if(mobileNumber.test(text) && (text.length > 9)){
                //console.warn("Cool Number");
                this.setState(prevState => ({
                    success: {
                        ...prevState.success,
                        mobSuccess: true
                    }}))
                    this.setState({number : text})
            }else{
              //console.warn("Bad Number");
              this.setState(prevState => ({
                success: {
                    ...prevState.success,
                    mobSuccess: false
                }}))
            }
          }else if(type=='emailAddress' ){
            if(emailPattern.test(text)&& (text.length > 0)){
                //console.warn("Cool Email");
                this.setState(prevState => ({
                    success: {
                        ...prevState.success,
                        emailSuccess: true
                    }}))
                    this.setState({email : text})
            }else{
              //console.warn("Bad Email");
              this.setState(prevState => ({
                success: {
                    ...prevState.success,
                    emailSuccess: false
                }}))
            }
          }else if(type=='confirmPassword'){
            if(text == this.state.password && (text.length > 0)){
                //console.warn("password doesn't match")
                this.setState(prevState => ({
                    success: {
                        ...prevState.success,
                        confirmPasswordSuccess: true
                    }}))
                    this.setState({password : text})
            }else{
                //console.warn("password matched")
                this.setState(prevState => ({
                    success: {
                        ...prevState.success,
                        confirmPasswordSuccess: false
                    }}))
            }
          }else if(type == 'password'){
              if(text.length > 4){
                this.setState({password : text})
                this.setState(prevState => ({
                    success: {
                        ...prevState.success,
                        passwordSuccess: true
                    }}))
              }else{
                this.setState(prevState => ({
                    success: {
                        ...prevState.success,
                        passwordSuccess: false
                    }}))
              }
          }
      }
    
      async _onPressButton() {
        var success = this.state.success
        this.setState({
            visibleSpinner: true
        });
        if (success.nameSuccess && success.mobSuccess && success.emailSuccess && success.passwordSuccess && success.confirmPasswordSuccess ){
            await this.setState({validated : true})

            let data = 
                JSON.stringify({
                    userName : this.state.name,
                    password : this.state.password,
                    email : this.state.email,
                    mobile : this.state.mobile
                })
            
            serviceCall('POST',data)
                .then((data) => {
                    console.warn(data.json.response);
                    this.setState({ visibleSpinner: false });
                    this.props.navigation.goBack();
                }).catch(() => {
                        this.setState({ visibleSpinner: false });
                        console.error("Api call error");
                    });
        }
        
      }

    render(){
        
        return(
            <Container>
                <Spinner visible={this.state.visibleSpinner} textContent={"Loading..."} textStyle={{color: '#FFF'}} />

                <Grid>

                    <Row size={8}>
                        <Content>
                            <Form style={{
                                    flex: 1,
                                    flexDirection: 'column',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    }}>
                            
                            <Item floatingLabel style={{width:'80%',marginTop:10}} success={this.state.success.nameSuccess} error={!this.state.success.nameSuccess}>
                                <Label style={{color:'gray'}}>Your Name</Label>
                                <Input onChangeText = {(text) => this.validate(text, 'yourName')}/>
                            </Item>

                            <Item floatingLabel style={{width:'80%',marginTop:10}} success={this.state.success.mobSuccess} error={!this.state.success.mobSuccess}>
                                <Label style={{color:'gray'}}>Mobile No.</Label>
                                <Input keyboardType = 'phone-pad' onChangeText = {(number) => this.validate(number, 'mobileNumber')}/>
                            </Item>

                            <Item floatingLabel style={{width:'80%',marginTop:10}} success={this.state.success.emailSuccess} error={!this.state.success.emailSuccess}>
                                <Label style={{color:'gray'}}>Email</Label>
                                <Input keyboardType = 'email-address' onChangeText = {(email) => this.validate(email, 'emailAddress')}/>
                            </Item>

                            <Item floatingLabel style={{width:'80%',marginTop:10}} success={this.state.success.passwordSuccess} error={!this.state.success.passwordSuccess} >
                                <Label style={{color:'gray'}}>Enter a password</Label>
                                <Input secureTextEntry onChangeText = {(password) => this.validate(password, 'password') } />
                            </Item>

                            <Item floatingLabel style={{width:'80%',marginTop:10}} success={this.state.success.confirmPasswordSuccess} error={!this.state.success.confirmPasswordSuccess}>
                                <Label style={{color:'gray'}}>Repeat the password</Label>
                                <Input secureTextEntry onChangeText = {(confirmPassword) => this.validate(confirmPassword, 'confirmPassword')} />
                            </Item>
                            
                        </Form>

                        
                                    
                    </Content>
                </Row>
                <Row size={2} style={{ flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        }}>
                    <Button style={{width:'80%', alignSelf:'center', marginBottom:10}}
                                        full
                                        rounded
                                        primary
                                        onPress = {this._onPressButton.bind(this)}>
                    <Text style={{color:'white'}}>Sign Up</Text>        
                    </Button>
                </Row>

            </Grid>
        </Container>
        );
    }

}
