import React from 'react';
import { Container,Text, Button, Textarea,Form, Content,Input,Item,Icon} from 'native-base';
let index = 0 ; 

export default class AddQuestion extends React.Component {
      constructor(props){
        super(props);
        this.state = {
          rows: [],
          
        };
      }

    _addRow = () => {
      this.state.rows.push(index++);
      this.setState({ rows: this.state.rows });
    }

    _deleteRow = (key) => {
      console.log(key)
      this.state.rows.splice(key, 1)
      this.setState({ rows: this.state.rows });
    }


    static navigationOptions = ({ navigation }) => {
      return {
          title: 'Ask a question',
          headerRight: <Icon style={{ marginRight:15,color:'red' }} name={'close'} size={55} onPress={() => navigation.goBack()}/>,
          headerLeft: null,
          
      };
   };

    render() {
      let rows = this.state.rows.map((r, i) => {
        return <Content key={ i } style={{height:70}} >
                  <Item rounded  key={ i } style={{ top : 20 , height : 50}}>
                      <Input placeholder = {'answer'}/>
                      <Icon style={{color:'red' }} name={'remove-circle'} size={25} onPress={this._deleteRow.bind(this,i)}/>
                  </Item>
              </Content>
    })
      return (
        <Container style={{
          flex: 1
        }} >
      
        <Content padder style={{ top : 20}}>
          <Form>
            <Text>Question:</Text>
            <Textarea rowSpan={5} bordered placeholder="Add a question here" />
            <Text style={{ top : 10}}>Answers:</Text>
            <Content>
                { rows }
            </Content>
            <Item rounded  style={{ top : 20 , height : 50}}>
                  <Input placeholder='answer'/>
                  <Icon style={{color:'blue' }} name={'add-circle'} size={25} onPress={this._addRow}/>
            </Item>
          </Form>
        </Content>
        <Button style={{alignSelf:'center'}} onPress={() => this.props.navigation.navigate("SelectFriendsAndGroups")}>
          <Text>Ask a Question</Text>
        </Button>
      </Container>
      );
    }
}