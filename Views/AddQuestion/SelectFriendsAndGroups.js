import React from "react";
import FriendsTab from "./FriendsTab";
import GroupTab from "./GroupTab";
import { TabNavigator, TabBarBottom } from "react-navigation";

import { Icon, Container, Grid, Row, Button, Text } from "native-base";
import {
  SCLAlert,
  SCLAlertButton
} from 'react-native-scl-alert'

export default class SelectFriendsAndGroups extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
    });
    this.setState({ loading: false });
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: "Select Contacts"
    };
  };

  addQuestionAnswer = () => {
    this.props.navigation.navigate("AddQuestion");
  };
  handleOpen = () => {
    this.setState({ show: true })
  }

  handleClose = () => {
    this.setState({ show: false })
  }

  render() {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }
    return (
      <Container>
        <Grid>
          <Row size={9.25}>
            <SelectFriendsAndGroupsStack />
          </Row>
          <Row size={0.75}>
            <Row>
              <Button full success style={{ width: "100%", marginBottom: 0 }} onPress={this.handleOpen}>
                <Text>SEND</Text>
              </Button>
            </Row>
          </Row>
        </Grid>
        <SCLAlert
          theme="success"
          show={this.state.show}
          title="Success"
          subtitle="Question has been sent to contacts. "
          onRequestClose = {() => {}}
          slideAnimationDuration = {350}
        >
          <SCLAlertButton theme="success" onPress={this.handleClose}>Done</SCLAlertButton>
        </SCLAlert>
      </Container>
    );
  }
}

SelectFriendsAndGroupsStack = TabNavigator(
  {
    Friends: { screen: FriendsTab },
    Groups: { screen: GroupTab }
  },
  {
    tabBarPosition: "top",
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === "Groups") {
          iconName = `contacts`;
          iconColor = focused ? "tomato" : "gray";
        } else if (routeName === "Friends") {
          iconName = "contact";
          iconColor = focused ? "tomato" : "gray";
        }
        return <Icon name={iconName} size={25} style={{ color: iconColor }} />;
      }
    }),
    tabBarComponent: TabBarBottom,
    tabBarOptions: {
      activeTintColor: "tomato",
      inactiveTintColor: "gray"
    },
    animationEnabled: true,
    swipeEnabled: true
  }
);
