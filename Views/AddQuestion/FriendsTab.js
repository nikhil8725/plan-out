import React from "react";
import {
  Container,
  Content,
  Left,
  Body,
  Text,
  List,
  ListItem,
  Icon,
  Button,
} from "native-base";
import { ListView,Alert } from "react-native";

const datas = [
  "Simon Mignolet",
  "Nathaniel Clyne",
  "Dejan Lovren",
  "Mama Sakho",
  "Alberto Moreno",
  "Emre Can",
  "Joe Allen",
  "Phil Coutinho"
];
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
export default class FriendsTab extends React.Component {
  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      loading: true,
      dataSource: ds.cloneWithRows([]),
      listViewData: datas
    };
  }
  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
    });
    this.setState({ loading: false });
  }

  deleteRow(secId, rowId, rowMap) {
    Alert.alert(
      'Do you really want to delete '+this.state.listViewData[rowId] +' ?',
      '',
      [
        {text: 'NO', onPress: () => console.log('No Pressed'), style: 'cancel'},
        {text: 'YES', onPress: () => this.deleteRowFunction(rowMap, secId, rowId)},
      ],
      { cancelable: false }
    )

    
  }

  deleteRowFunction(rowMap, secId, rowId) {
    rowMap[`${secId}${rowId}`].props.closeRow();
    const newData = [...this.state.listViewData];
    newData.splice(rowId, 1);
    this.setState({ listViewData: newData });
  }

  render() {
    var questions = [
      "friend1",
      "friend2",
      "friend3",
      "friend4",
      "friend1",
      "friend2",
      "friend3",
      "friend4",
      "friend1",
      "friend2",
      "friend3",
      "friend4",
      "friend1",
      "friend2",
      "friend3",
      "friend4"
    ];
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }

    return (
      <Container>
        <Content style={{ marginTop: 0 }}>
          <List
            rightOpenValue={-75}
            dataArray={questions}
            dataSource={this.ds.cloneWithRows(this.state.listViewData)}
            renderRow={item => (
              <ListItem avatar>
                <Left>
                  <Icon name="ios-checkmark-circle" style={{ color: "blue" }} />
                </Left>
                <Body>
                  <Text>{item}</Text>
                </Body>
              </ListItem>
            )}
            
            renderRightHiddenRow={(data, secId, rowId, rowMap) => (
              <Button
                full
                danger
                onPress={_ => this.deleteRow(secId, rowId, rowMap)}
              >
                <Icon active name="trash" />
              </Button>
            )}
          />
        </Content>
      </Container>
    );
  }
}
