import React from "react";
import {
  Container,
  Content,
  Left,
  Body,
  Text,
  List,
  ListItem,
  Icon
} from "native-base";

export default class QuestionHome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }
  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
    });
    this.setState({ loading: false });
  }
  render() {
    var questions = ["John", "Alex", "Dino", "Robert"];
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }

    return (
      <Container>
        <Content style={{ marginTop: 30 }}>
          <List
            dataArray={questions}
            renderRow={item => (
              <ListItem avatar>
                <Left>
                  <Icon
                    name="ios-checkmark-circle"
                    style={{ color: "tomato" }}
                  />
                </Left>
                <Body>
                  <Text>{item}</Text>
                </Body>
              </ListItem>
            )}
          />
        </Content>
      </Container>
    );
  }
}
