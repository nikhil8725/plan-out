import React from "react";
import AnswerHome from "./AnswerHome.js";
import QuestionHome from "./QuestionHome";
import { TabNavigator,TabBarBottom} from "react-navigation";

import {
  Icon,
  Container} from "native-base";



export default class Home extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      userName: '',
      password : '',
      loading: true
    };
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
    });
    this.setState({ loading: false });
    
  }


  static navigationOptions = ({ navigation }) => {
    return {
        title: 'Home',
        headerRight: <Icon style={{ marginRight:15,color:'red' }} name={'add-circle'} size={25} onPress={() => navigation.navigate('AddQuestion')}/>,
        headerLeft: <Icon name="ios-menu" style={{ paddingLeft: 10 }} onPress={() => navigation.navigate('DrawerOpen')} />,
        
    };
 };

  addQuestionAnswer = () => {
    this.props.navigation.navigate("AddQuestion")
  }

  render() {
    if (this.state.loading) {
        return <Expo.AppLoading />;
      } 
    return (
        <Container>
          <HomeStack/>
        </Container>
  
    );
  }
}


(HomeStack = TabNavigator(
  {
    Question: { screen: QuestionHome },
    Answer: { screen: AnswerHome }
  },
  {
    tabBarPosition: "bottom",
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconName;
         if (routeName === 'Answer') {
          iconName = `ios-checkmark-circle${focused ? '' : '-outline'}`;
          iconColor = focused ? 'tomato' : 'gray'
        }else   if (routeName === 'Question') {
          iconName = `ios-help-circle${focused ? '' : '-outline'}`;
          iconColor = focused ? 'tomato' : 'gray'
        } 
        return <Icon name={iconName} size={25} style={{color:iconColor}} />;
      },
    }),
    tabBarComponent: TabBarBottom,
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
    animationEnabled: true,
    swipeEnabled: true,
  }
));