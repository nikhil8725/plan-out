import React from 'react';
import { Container, Content, Text,List,ListItem} from 'native-base';


export default class AnswerHome extends React.Component {
    render() {
      return (

        <Container>
        <Content>
          <List>
            <ListItem>
              <Text>Simon Mignolet</Text>
            </ListItem>
            <ListItem>
              <Text>Nathaniel Clyne</Text>
            </ListItem>
            <ListItem>
              <Text>Dejan Lovren</Text>
            </ListItem>
          </List>
        </Content>
      </Container>
      );
    }
}