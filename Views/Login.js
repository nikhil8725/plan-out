import React  from 'react';
import { Text } from 'react-native';
import { Container, Form, Item, Input, Button } from 'native-base';
import themeStyles from '../styles/styles.js';

export default class Login extends React.Component {
  static navigationOptions = {
    title: 'LOGIN',
  };

  constructor(props){
    super(props);
    this.state = {
      userName: '',
      password : '',
      loading: true
    };
    

  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
    });
    this.setState({ loading: false });
    
  }

  onLoginClick = () => {
    //Alert.alert(this.state.userName + this.state.password);
    //await AsyncStorage.setItem('userToken', 'abc');
    this.props.navigation.navigate("App")
  }

  onSignUpClick = () => {
    this.props.navigation.navigate("SignUp")
  }

  render() {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    } 
    return (
    <Container style={{
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    }}>

          <Form style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                       
                      }} >
            <Item rounded style={{width:'80%',marginTop:10}} >
              <Input placeholder="Email"/>
            </Item>
            <Item rounded style={{width:'80%',marginTop:10}} >
              <Input placeholder="Password"  />
            </Item>
            <Button   
                style={{marginTop:10}}
                full
                rounded
                primary
                onPress={this.onLoginClick}>
            <Text style={{color: 'white'}}>LOGIN</Text>
          </Button>
          
              <Text style={{color:'gray', marginTop:8, marginLeft:10}}>Are you new here? </Text>
              <Text onPress = {this.onSignUpClick} style={{color : themeStyles.PRIMARY_COLOR, marginTop:3}}>Sign Up! </Text>

          </Form>
         
      </Container>
    );
  }
}

