import React, { Component } from "react";
import { Container,Text, Button,Icon} from 'native-base';

export default class Home extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Settings',
            headerLeft: <Icon name="ios-menu" style={{ paddingLeft: 10 }} onPress={() => navigation.navigate('DrawerOpen')} />,
            
        };
     };

    render() {
      
        return (
            <Container style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        
            <Button style={{alignSelf:'center'}}
              onPress={() => this.props.navigation.goBack()}
             
            ><Text>Settings Screen</Text>
            </Button>
          </Container>
      
        );
      }
}